//bibliotecas 
let express = require('express');
let body_parser = require('body-parser');

/** Configurações do servidor */
let app = express();

/** Processamento de Contente type
 * application/json e x-www-form-url-encoded
 */
app.use(body_parser.urlencoded({extended:false}));
app.use(body_parser.json());

/**
 * Referencia e configuração de rotas 
 */
 const attendanceRouter = require('../routes/AttendanceRouter');
 
 /** Registro das rotas */
 app.use("/atendimento",attendanceRouter);

 module.exports = app;
