# Processo Seletivo: Backend

Desafio com objetivo de criar uma API REST para facilitar o gerenciamento de horários de uma clínica! a API contem endpoints para satisfazer as seguintes features:

    - Cadastrar regras de horários para atendimento
    - Apagar regra de horário para atendimento
    - Listar regras de horários para atendimento
    - Listar horários disponíveis dentro de um intervalo

Foram utilizados as seguintes tecnologias:
 **NODE JS** /
 **EXPRESS**

 ## Iniciando Projeto:

Abra o terminal, e com o mesmo acesse o diretorio do projeto, utilize o comando **NPM INSTALL** para baixar todas dependencias do projeto, apos isso utilize o comando **NODE INDEX**
para executar.

## Endpoints:

### Cadastro de regra de atendimento

O cadastro de regras de horário para atendimento possibilita as seguintes regras :

#### Dia Especifico
- exemplo: estará disponível para atender dia 25/06/2018 nos intervalos de 9:30 até 10:20 e de 10:30 até as 11:00
- padrao de dado para cadastro:
```
{
"0":{
	    "day": "15-01-2018",
	    "intervals": [
	       {
	            "start": "10:30",
	            "end": "11:30"
	        },
	        {
	            "start": "12:30",
	            "end": "13:30"
	        }
	    ]
	}
}
```
- caso queira inserir mais de um em uma mesma requisiçao:
```
{
"0":{
	    "day": "15-01-2018",
	    "intervals": [
	        {
	            "start": "10:30",
	            "end": "11:30"
	        },
	        {
	            "start": "12:30",
	            "end": "13:30"
	        }
	    ]
	},
"1":{
    "day": "25-01-2018",
    "intervals": [
        {
            "start": "10:30",
            "end": "11:30"
        },
        {
            "start": "12:30",
            "end": "13:30"
        }
    ]
}
}
```
- url para cadastro:
```
 http://localhost:3000/atendimento/cadastrar/diaEspecifico
```

#### Diariamente

- exemplo: estará disponível para atender todos os dias das 9:30 até as 10:10
- padrao de dado para cadastro:

```
{
    "intervals": [
        {
            "start": "10:30",
            "end": "11:30"
        },
        {
            "start": "12:30",
            "end": "13:30"
        }
    ]
}
```
- url para cadastro:
```
 http://localhost:3000/atendimento/cadastrar/diariamente
```

-obs: Nao nescessita colocar o dado day, pois esse tipo de cadastro sera para todos os dias


#### Semanalmente
- exemplo: estará disponível para atender todas segundas e quartas das 14:00 até as 14:30
- padrao de dado para cadastro:


```
{
    "0": {
            "day": "domingo",
            "intervals": [
                {
                    "start": "10:30",
                    "end": "11:30"
                },
                {
                    "start": "12:30",
                    "end": "13:30"
                }
            ]
        }
}
```
- caso queira inserir mais de um em uma mesma requisiçao:

```
{
	"0": {
            "day": "domingo",
            "intervals": [
                {
                    "start": "10:30",
                    "end": "11:30"
                },
                {
                    "start": "12:30",
                    "end": "13:30"
                }
            ]
        },
        "1": {
            "day": "sabado",
            "intervals": [
                {
                    "start": "10:30",
                    "end": "11:30"
                },
                {
                    "start": "12:30",
                    "end": "13:30"
                }
            ]
        }
}
```
- url para cadastro:
```
 http://localhost:3000/atendimento/cadastrar/semanalmente
```
### Apagar regra

A API e capaz de apagar uma regra especifica criada pelo endpoint descrito em "Cadastro de regra de atendimento".

- exemplo diariamente:

```
 http://localhost:3000/atendimento/deletar/diariamente
```
- obs: o diariamente ira apagar todos os dias cadastrados como diariamente

- exemplo dia especifico:

```
 http://localhost:3000/atendimento/deletar/diaEspecifico/15-01-2018
```
- exemplo semanalmente:

```
 http://localhost:3000/atendimento/deletar/semanalmente/segunda-feira
```

### Listar regras

A API e capaz de listar e retornar as regras de atendimento criadas pelo endpoint descrito em "Cadastro de regra de atendimento".

- exemplo:
 ```
 http://localhost:3000/atendimento/listar/horarios/todos
```
### Horários disponíveis

A API e capaz de retornar os horários disponíveis, baseado nas regras criadas anteriormente, considerando um intervalo de datas informadas na requisição.

- exemplo:
 ```
 http://localhost:3000/atendimento/listar/horarios/25-01-2018/26-01-2018
```
### Obs Gerais

o codigo foi escrito em ingles como normalmente e de padrao, porem as rotas e comentarios foram escritos em portugues(Brasil), normalmente existe um padrao
para que codigos sejam escritos em apenas uma linguagem, porem para o melhor entendimento da avaliaçao as rotas que ficam acessiveis ao usuario estao em portugues e os comentarios do codigo tambem.
