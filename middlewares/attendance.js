/**
 * Middleware de validaçao verificando se o parametro passado e do tipo JSON e nao esta vazio
 */
const validateParameters = (req, res, next) => {
    try {
        var titleError = "Parâmetro incorreto";
        var statusError = 400;
        
        /** verifica se o tipo do parametro e Json, caso ocorra um erro cai na condiçao catch */
        let data = JSON.stringify(req.body);
        data = JSON.parse(data);

        /** verifica se o parametro esta vazio, caso esteja lança um erro */
        if (Object.keys(data).length === 0) {
            throw "Tipo de dado invalido!";
        } else {
            /** verifica o tipo da operaçao, se e de dia especifico, diariamente ou semanalmente e valida os dados especificos de cada um */
            switch (req.params.type) {
                case "diaEspecifico":
                    Object.keys(data).forEach(element => {
                        if (data[element].day) {
                            let matches = /(\d{2})[-](\d{2})[-](\d{4})/.exec(data[element].day);
                            if (matches == null) {
                                throw "Formato de data invalido, padrao aceitavel dd-mm-yyyy!";
                            }
                            let day = matches[1];
                            let month = matches[2] - 1;
                            let year = matches[3];
                            let date = new Date(year, month, day);
                            if (date.getDate() == day && date.getMonth() == month && date.getFullYear() == year) {
                                if (data[element].intervals.length == 0) {
                                    throw "Erro de dado para Dia especifico, dado de parametro invalido!";
                                }
                            } else {
                                throw "Erro de dado para Dia especifico, dado de parametro invalido!";
                            }
                        } else {
                            throw "Erro de dado para Dia especifico, o processo precisa de dados que nao foram encontrados nos parametros!";
                        }
                    });
                    next();
                    break;
                case "diariamente":
                    if (data.intervals.length === 0) {
                        throw "Erro de dado para Diariamente, dado de parametro invalido!";
                    }
                    next();
                    break;
                case "semanalmente":
                    Object.keys(data).forEach(element => {
                        if (data[element].day) {
                            if (data[element].intervals.length == 0) {
                                throw "Erro de dado para Semanalmente, dado de parametro invalido!";
                            }
                        } else {
                            throw "Erro de dado para Semanalmente, o processo precisa de dados que nao foram encontrados nos parametros!";
                        }
                    });
                    next();
                    break;
                default:
                    titleError = "Url invalida";
                    statusError = 404;
                    throw "Nao consegui identificar a rota dessa url";
            }
        }

    } catch (ex) {
        res.status(statusError).json({
            "status": statusError,
            "error": {
                "title": titleError,
                "message": ex
            }
        })
    }
}

validateExclusionParameters= (req, res, next) => {
    var titleError = "Erro ao excluir linha";
    var statusError = 400;
    try{
        switch (req.params.type) {
            case "diaEspecifico":
                if(req.params.target != ""){
                    let matches = /(\d{2})[-](\d{2})[-](\d{4})/.exec(req.params.target);
                    if (matches == null) {
                        throw "Formato de data invalido, padrao aceitavel dd-mm-yyyy!";
                    }
                    let day = matches[1];
                    let month = matches[2] - 1;
                    let year = matches[3];
                    let date = new Date(year, month, day);
                    if (date.getDate() == day && date.getMonth() == month && date.getFullYear() == year) {
                        
                       next();
                    }else{
                        throw "Dia especifico, dado de parametro invalido!";
                    }
                }else{
                    throw "Dia especifico, dado de parametro invalido!";
                }
                
                break;
            case "diariamente":
                
                next();
                break;
            case "semanalmente":
                let week = ["domingo", "segunda-feira", "terça-feira", "quarta-feira", "quinta-feira", "sexta-feira", "sabado"];
                let dateTarget = req.params.target.toLowerCase();
                if (week.includes(dateTarget)) {
                    next();
                }else{
                    throw "Semanalmente, dado de parametro invalido!";
                }
                break;
            default:
                titleError = "Url invalida";
                statusError = 404;
                throw "Nao consegui identificar a rota dessa url";
        }
    }catch (ex) {
        res.status(statusError).json({
            "status": statusError,
            "error": {
                "title": titleError,
                "message": ex
            }
        })
    }
    
}

module.exports = {
    validateParameters,
    validateExclusionParameters
}