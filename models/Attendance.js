let fs = require('fs');
class Attendance {
    createService = (archive, attendanceRule) => {
        return new Promise((resolve, reject) => {
            attendanceRule = JSON.stringify(attendanceRule);
            fs.writeFile(`./data/${archive}.json`, attendanceRule, function (err) {
                if (err) {
                    reject(false);
                } else {
                    resolve(true);
                }
            });
        });
    }
    readArchive = (archive) => {
        return new Promise((resolve, reject) => {
            fs.readFile(`./data/${archive}.json`, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }
    
    findSchedules = () => {
        return new Promise((resolve, reject) => {
            fs.readFile(`./data/${archive}.json`, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }
}

module.exports = new Attendance();