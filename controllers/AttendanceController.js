const attendance = require('../models/Attendance');

class AttendanceController {
    async registerServiceDaily(req, res) {
        let archive = "";
        switch (req.params.type) {
            case "diaEspecifico":
                archive = "SpecificDayData";
                break;
            case "diariamente":
                archive = "DailyData";
                break;
            case "semanalmente":
                archive = "WeeklyData";
                break;
        }

        let attendanceRule = "";

        /** Se nao houve dado no arquivo insere o primero, começando pelo identificador 0 */
        let dataJson = await attendance.readArchive(`${archive}`);
        if (dataJson.toString() == "") {
            attendanceRule = {
                "0": req.body
            }
        } else {
            /** Se houve dado no arquivo insere o dado da requisiçao */
            dataJson = JSON.parse(dataJson);
            let lastPosition = "";
            Object.keys(dataJson).forEach((last) => {
                lastPosition = last
            });
            lastPosition++;
            dataJson[lastPosition] = req.body;
            attendanceRule = dataJson;
        }

        let response = await attendance.createService(`${archive}`, attendanceRule);
        if (response) {
            res.status(200).json({
                "status": 200,
                "message": "Dados cadastrados com sucesso!"
            });
        } else {
            res.status(500).json({
                "status": 500,
                "message": "Ops, houve um erro ao cadastrar os dados, entre em contato com o suporte!"
            });
        }
    }

    async fetchSchedules(req, res) {
        /** Quebra a data inicial em um array */
        let dateStartArray = req.params.start.split("-");
        let dayStart = dateStartArray["0"];
        let monthStart = dateStartArray["1"];
        let yearStart = dateStartArray["2"];

        /** Quebra a data final em um array */
        let dateEndArray = req.params.end.split("-");
        let dayEnd = dateEndArray["0"];
        let monthEnd = dateEndArray["1"];
        let yearEnd = dateEndArray["2"];

        /** Verifica quantos dias existe entre a data inicial e a data final */
        const dateStart = new Date(`${monthStart}/${dayStart}/${yearStart}`);
        const dateEnd = new Date(`${monthEnd}/${dayEnd}/${yearEnd}`);
        const diffTime = Math.abs(dateEnd - dateStart);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        let d = new Date(yearStart, (monthStart - 1), dayStart);

        /** Chamada de funçoes para leitura de arquivos de dados */
        let dataSpecificDay = await attendance.readArchive("SpecificDayData");
        let dataDaily = await attendance.readArchive("DailyData");
        let dataWeekly = await attendance.readArchive("WeeklyData");

        /** Verifica se algum dos arquivos de dados esta vazio, se nao tranforma em objeto json */
        if (dataWeekly.toString() == "") {
            dataWeekly = 0;
        } else {
            dataWeekly = JSON.parse(dataWeekly.toString());
        }

        if (dataDaily.toString() == "") {
            dataDaily = 0;
        } else {
            dataDaily = JSON.parse(dataDaily.toString());
        }

        if (dataSpecificDay.toString() == "") {
            dataSpecificDay = 0;
        } else {
            dataSpecificDay = JSON.parse(dataSpecificDay.toString());
        }

        let attendanceDataArray = [];

        /** Verifica se em dados de atendimento diario existe associaçao com as datas solicitadas pelo usuario */
        if (Object.keys(dataDaily).length != 0) {
            for (let i = 0; i <= diffDays; i++) {
                if (i != 0) {
                    d.setDate(d.getDate() + 1);
                }
                attendanceDataArray.push({
                    "day": `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()} `
                })
            }
            var intervalsArray = []
            attendanceDataArray.forEach(dda => {
                dda.intervals = [];
                Object.keys(dataDaily).forEach(element => {
                    intervalsArray.push(dataDaily[element].intervals);
                    dataDaily[element].intervals.forEach(dtdl => {
                        dda.intervals.push(dtdl);
                    });

                });
            })
        }

        /** Verifica se em dados de atendimento semanal existe associaçao com as datas solicitadas pelo usuario */
        if (Object.keys(dataWeekly).length != 0) {
            let week = ["domingo", "segunda-feira", "terça-feira", "quarta-feira", "quinta-feira", "sexta-feira", "sabado"];
            if (attendanceDataArray.length == 0) {
                d = new Date(yearStart, (monthStart - 1), dayStart);
                for (let i = 0; i <= diffDays; i++) {
                    if (i != 0) {
                        d.setDate(d.getDate() + 1);
                    }
                    Object.keys(dataWeekly).forEach((dtwk) => {
                        Object.keys(dataWeekly[dtwk]).forEach((dtwkData) => {
                            var dateWekly = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            var dtW = dateWekly.getDay();
                            if (dataWeekly[dtwk][dtwkData].day.toLowerCase() == week[dtW].toLowerCase()) {
                                attendanceDataArray.push({
                                    "day": `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()} `,
                                    "intervals": dataWeekly[dtwk][dtwkData].intervals
                                })
                            }
                        })
                    });
                }
            } else {
                d = new Date(yearStart, (monthStart - 1), dayStart);
                for (let i = 0; i <= diffDays; i++) {
                    if (i != 0) {
                        d.setDate(d.getDate() + 1);
                    }
                    Object.keys(dataWeekly).forEach((dtwk) => {
                        Object.keys(dataWeekly[dtwk]).forEach((dtwkData) => {
                            var dateWekly = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            var dtW = dateWekly.getDay();
                            if (dataWeekly[dtwk][dtwkData].day.toLowerCase() == week[dtW].toLowerCase()) {
                                attendanceDataArray.forEach((element) => {
                                    let attdDayArray = element.day.split("-")
                                    if (attdDayArray[0] == d.getDate() && attdDayArray[1] == (d.getMonth() + 1) && attdDayArray[2] == d.getFullYear()) {
                                        dataWeekly[dtwk][dtwkData].intervals.forEach((l) => {
                                            element.intervals.push(l);
                                        });
                                    }
                                })
                            }
                        })
                    });
                }
            }

        }
        /** Verifica se em dados de atendimento de dia especifico existe associaçao com as datas solicitadas pelo usuario */
        if (Object.keys(dataSpecificDay).length != 0) {
            if (attendanceDataArray.length == 0) {
                d = new Date(yearStart, (monthStart - 1), dayStart);
                for (let i = 0; i <= diffDays; i++) {
                    if (i != 0) {
                        d.setDate(d.getDate() + 1);
                    }
                    Object.keys(dataSpecificDay).forEach(element => {
                        Object.keys(dataSpecificDay[element]).forEach(dataElement => {
                            let dateSpecificDayJsonArray = dataSpecificDay[element][dataElement].day.split("-");
                            let dateSpecificDayJsonDay = dateSpecificDayJsonArray[0];
                            let dateSpecificDayJsonMonth = dateSpecificDayJsonArray[1];
                            let dateSpecificDayJsonYear = dateSpecificDayJsonArray[2];
                            if (dateSpecificDayJsonDay == d.getDate() && dateSpecificDayJsonMonth == (d.getMonth() + 1) && dateSpecificDayJsonYear == d.getFullYear()) {
                                Object.keys(dataSpecificDay[element]).forEach(dtsdData => {
                                    attendanceDataArray.push(dataSpecificDay[element][dtsdData]);
                                });
                            }
                        });
                    });
                }

            } else {
                d = new Date(yearStart, (monthStart - 1), dayStart);
                var dataToInsertLater = [];
                for (let i = 0; i <= diffDays; i++) {
                    if (i != 0) {
                        d.setDate(d.getDate() + 1);
                    }
                    Object.keys(dataSpecificDay).forEach(element => {
                        Object.keys(dataSpecificDay[element]).forEach(dataElement => {
                            let dateSpecificDayJsonArray = dataSpecificDay[element][dataElement].day.split("-");
                            let dateSpecificDayJsonDay = dateSpecificDayJsonArray[0];
                            let dateSpecificDayJsonMonth = dateSpecificDayJsonArray[1];
                            let dateSpecificDayJsonYear = dateSpecificDayJsonArray[2];
                            if (dateSpecificDayJsonDay == d.getDate() && dateSpecificDayJsonMonth == (d.getMonth() + 1) && dateSpecificDayJsonYear == d.getFullYear()) {
                                attendanceDataArray.forEach((attd) => {
                                    let attdArray = attd.day.split("-");
                                    if (dateSpecificDayJsonDay == attdArray[0] && dateSpecificDayJsonMonth.replace("0", "") == attdArray[1] && dateSpecificDayJsonYear == d.getFullYear()) {
                                        dataSpecificDay[element][dataElement].intervals.forEach((dtsdData) => {
                                            attd.intervals.push(dtsdData);
                                        })
                                    } else {
                                        dataToInsertLater.push(dataSpecificDay[element]);
                                    }
                                })

                            }
                        });

                    });

                }
            }
        }
        res.status(200).json(attendanceDataArray)
    }
    async deleteSchedules(req, res) {
        let archive = "";
        let targetExclusion = "";
        let response = "";
        let found = "";
        let statusCode = "";
        let message = "";
        switch (req.params.type) {
            case "diaEspecifico":
                archive = "SpecificDayData";
                found = false;
                targetExclusion = req.params.target;
                let dataSpecificDay = await attendance.readArchive(archive);
                if (dataSpecificDay.toString() == "") {
                    dataSpecificDay = 0;
                } else {
                    dataSpecificDay = JSON.parse(dataSpecificDay.toString());
                }

                if (dataSpecificDay != 0) {
                    Object.keys(dataSpecificDay).forEach(element => {
                        Object.keys(dataSpecificDay[element]).forEach(dtsp => {
                            if (dataSpecificDay[element][dtsp].day == targetExclusion) {
                                if (Object.keys(dataSpecificDay[element]).length == 1) {
                                    found = true;
                                    delete dataSpecificDay[element];
                                } else {
                                    delete dataSpecificDay[element][dtsp];
                                }
                            }
                        })
                    })
                    if (found == false) {
                        statusCode = 400;
                        message = "Dado nao encontrado";
                    } else {
                        response = await attendance.createService(archive, dataSpecificDay);
                        if (response) {
                            statusCode = 200;
                            message = "Dado excluido com sucesso";
                        } else {
                            statusCode = 500;
                            message = "Erro ao excluir dado, por favor entre em contato com o suporte";
                        }
                    }
                } else {
                    statusCode = 400;
                    message = "Dado nao encontrado";
                }
                res.status(statusCode).json({
                    "status": statusCode,
                    "message": message
                })
                break;
            case "diariamente":
                archive = "DailyData";
                statusCode = "";
                response = await attendance.createService(archive, {});
                if (response) {
                    statusCode = 200;
                    message = "Dado excluido com sucesso";
                } else {
                    statusCode = 500;
                    message = "Erro ao excluir dado, por favor entre em contato com o suporte";
                }
                res.status(statusCode).json({
                    "status": statusCode,
                    "message": message
                })
                break;
            case "semanalmente":
                found = false;
                archive = "WeeklyData";
                statusCode = "";
                let dataWeekly = await attendance.readArchive(archive);
                if (dataWeekly.toString() == "") {
                    dataWeekly = 0;
                } else {
                    dataWeekly = JSON.parse(dataWeekly.toString());
                }
                targetExclusion = req.params.target;
                if (dataWeekly != 0) {
                    Object.keys(dataWeekly).forEach(element => {
                        Object.keys(dataWeekly[element]).forEach(dtsp => {
                            if (dataWeekly[element][dtsp].day.toLowerCase() == targetExclusion.toLowerCase()) {
                                found = true;
                                if (Object.keys(dataWeekly[element]).length == 1) {
                                    delete dataWeekly[element];
                                } else {
                                    delete dataWeekly[element][dtsp];
                                }
                            }
                        })
                    })
                    if (found == false) {
                        statusCode = 400;
                        message = "Dado nao encontrado";
                    } else {
                        response = await attendance.createService(archive, dataWeekly);
                        if (response) {
                            statusCode = 200;
                            message = "Dado excluido com sucesso";
                        } else {
                            statusCode = 500;
                            message = "Erro ao excluir dado, por favor entre em contato com o suporte";
                        }
                    }

                } else {
                    statusCode = 400;
                    message = "Dado nao encontrado";
                }

                res.status(statusCode).json({
                    "status": statusCode,
                    "message": message
                })
                break;
        }
    }
    async listAllCalls(req, res) {
        let attendanceDataArray = [];

        /** Chamada de funçoes para leitura de arquivos de dados */
        let dataSpecificDay = await attendance.readArchive("SpecificDayData");
        let dataDaily = await attendance.readArchive("DailyData");
        let dataWeekly = await attendance.readArchive("WeeklyData");

        /** Verifica se algum dos arquivos de dados esta vazio, se nao tranforma em objeto json */
        if (dataWeekly.toString() == "") {
            dataWeekly = 0;
        } else {
            dataWeekly = JSON.parse(dataWeekly.toString());
        }

        if (dataDaily.toString() == "") {
            dataDaily = 0;
        } else {
            dataDaily = JSON.parse(dataDaily.toString());
        }

        if (dataSpecificDay.toString() == "") {
            dataSpecificDay = 0;
        } else {
            dataSpecificDay = JSON.parse(dataSpecificDay.toString());
        }

        /** Verifica se existem dados vazios para cada arquivo, se nao houve coloca como posiçao no array para exibir ao usuario */
        if (Object.keys(dataDaily).length != 0) {
            Object.keys(dataDaily).forEach(element => {
                dataDaily[element].day = "diariamente";
                attendanceDataArray.push(dataDaily[element]);
            });
        }

        if (Object.keys(dataWeekly).length != 0) {
            Object.keys(dataWeekly).forEach(element => {
                Object.keys(dataWeekly[element]).forEach(dtWeeklyData => {
                    attendanceDataArray.push(dataWeekly[element][dtWeeklyData]);
                })

            });
        }

        if (Object.keys(dataSpecificDay).length != 0) {
            Object.keys(dataSpecificDay).forEach(element => {
                Object.keys(dataSpecificDay[element]).forEach(dtSpecificDayData => {
                    attendanceDataArray.push(dataSpecificDay[element][dtSpecificDayData]);
                })

            });
        }
        res.status(200).json(attendanceDataArray);

    }
}

module.exports = new AttendanceController();