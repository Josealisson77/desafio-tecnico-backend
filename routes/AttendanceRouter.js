/** Referencias */
let express = require('express');
let router = express.Router();

/**
 *  Referenciando o controller Agendamento
 */

let attendanceController = require('../controllers/AttendanceController');

/**
 * Referenciando Middleware de Rotas
 */

let attendanceMiddleware = require("../middlewares/attendance");

/** 
 * Rotas Vinculadas aos seus repectivos metodos no controller
 */
router.post("/cadastrar/:type", attendanceMiddleware.validateParameters, attendanceController.registerServiceDaily);
router.get("/listar/horarios/:start/:end", attendanceController.fetchSchedules);
router.get("/listar/horarios/todos", attendanceController.listAllCalls);
router.delete("/deletar/:type/:target?", attendanceMiddleware.validateExclusionParameters, attendanceController.deleteSchedules);
module.exports = router;